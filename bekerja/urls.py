from django.urls import path
from.views import create, read, index, create_bekerja

urlpatterns = [
    path('', index, name='create-index'),
    path('create/', create, name='create-bekerja'),
    path('read/', read, name='read-bekerja'),
    path('create-bekerja/<str:nama_tokoh>/<str:pekerjaan>/<int:base_honor>/', create_bekerja, name='submit-create-bekerja'),
]