from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from datetime import datetime, timezone, timedelta


# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def read(request):
    if request.session.is_empty():
        return redirect("/")

    with connection.cursor() as c:
        role = request.session['role']
        username = request.session['username']
        if role == 'admin':
            c.execute('set search_path to thecims;')
            c.execute('select * from bekerja;')
            hasil = namedtuplefetchall(c)

        elif role == 'pemain':
            c.execute('set search_path to thecims;')
            c.execute("""select nama_tokoh, timestamp, nama_pekerjaan, keberangkatan_ke, honor from bekerja
            where username_pengguna = '{}';""".format(username))
            hasil = namedtuplefetchall(c)

    response = {
        'role': role,
        'hasil': hasil,
     }

    return render(request, 'bekerja-read.html', response)


def create(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")

    with connection.cursor() as c:
        username = request.session['username']
        c.execute('set search_path to thecims;')
        c.execute("""select t.nama as nama_tokoh, p.nama as nama_pekerjaan, base_honor from tokoh t, pekerjaan p
                    where t.pekerjaan=p.nama and username_pengguna = '{}'
                    order by t.nama asc;""".format(username))
        hasil = namedtuplefetchall(c)

    response = {
        'hasil': hasil
    }

    return render(request, 'bekerja-create.html', response)


def create_bekerja(request, nama_tokoh, pekerjaan, base_honor):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")

    username = request.session['username']

    with connection.cursor() as c:
        c.execute('set search_path to thecims;')
        c.execute("select max(keberangkatan_ke) as keberangkatan from bekerja where username_pengguna='{}' and nama_tokoh='{}';".format(username, nama_tokoh))
        keberangkatan_ke = namedtuplefetchall(c)[0].keberangkatan

    if keberangkatan_ke is None:
        keberangkatan_ke = 1
    else:
        keberangkatan_ke += 1

    with connection.cursor() as c:
        c.execute('set search_path to thecims;')
        c.execute("select level from tokoh where username_pengguna='{}' and nama='{}';".format(username, nama_tokoh))
        level = namedtuplefetchall(c)[0].level

    honor = base_honor * level
    dt = datetime.now(timezone.utc) + timedelta(hours=7)
    with connection.cursor() as c:
        c.execute('set search_path to thecims;')
        c.execute("insert into bekerja values ('{}', '{}', '{}', '{}', {}, {});".format(username, nama_tokoh, dt, pekerjaan, keberangkatan_ke, honor))

    return redirect('read-bekerja')


def index(request):
    if request.session.is_empty():
        return redirect("/")

    return render(request, 'bekerja-home.html')
