from django.urls import path
from .views import create,create_kategori, delete, read

urlpatterns=[
    path('read/',read,name='read'),
    path('create/',create,name='create'),
    path('create_kategori/',create_kategori,name='create_kategori'),
    path('delete/<str:nama_kategori>',delete,name='delete'),
]