from collections import namedtuple
from django.db import connection
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read(request):
    if(request.session.is_empty()):
        return HttpResponseRedirect('/')

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""select nama_kategori,
            case when nama_kategori in
            (select nama_kategori from kategori_apparel except (select kategori_apparel from apparel))
            then true
            else false
            end
            as delete
            from kategori_apparel;
        """)
        hasil = namedtuplefetchall(c)
        response = {"hasil":hasil}

        return render(request,"kategori_apparel_read.html",response)

def create(request):
    return render(request,"kategori_apparel_create.html")   

def create_kategori(request):
    if(request.session.is_empty() or request.session['role']=="pemain"):
        return HttpResponseRedirect('/')
    
    if request.method == "POST":
        nama_kategori = request.POST['nama_kategori']

        with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute("""
            insert into kategori_apparel values
            ('{}');
            """.format(nama_kategori))
    return HttpResponseRedirect("/kategori_apparel/read/")

def delete(request, nama_kategori):
    if(request.session.is_empty() or request.session['role']=="pemain"):
        return HttpResponseRedirect('/')
    
    with connection.cursor() as c:
        c.execute('set search_path to thecims;')
        c.execute("""
        delete from kategori_apparel
        where nama_kategori='{}';
        """.format(nama_kategori))
        
    return HttpResponseRedirect("/kategori_apparel/read/")
