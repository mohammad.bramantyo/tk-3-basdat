from django.urls import path

from koleksi.views import *

urlpatterns = [
    path('read/', read_list_koleksi,name='read_list_koleksi'),
    path('read/rambut/',read_rambut,name='read_rambut'),
    path('read/mata/',read_mata,name='read_mata'),
    path('read/rumah/',read_rumah,name='read_rumah'),
    path('read/barang/',read_barang,name='read_barang'),
    path('read/apparel/',read_apparel,name='read_appaerl'),
    path('create/',create_koleksi, name='create_koleksi'),
    path('create/rambut/',form_create_rambut, name='form_create_rambut'),
    path('create_rambut/',create_rambut, name='create_rambut'),
    path('create_mata/',create_mata, name='create_mata'),
    path('create_rumah/',create_rumah, name='create_rumah'),
    path('create_barang/',create_barang, name='create_barang'),
    path('create_apparel/',create_apparel, name='create_apparel'),
    path('create/mata/',form_create_mata, name='form_create_mata'),
    path('create/rumah/',form_create_rumah, name='form_create_rumah'),
    path('create/barang/',form_create_barang, name='form_create_barang'),
    path('create/apparel/',form_create_apparel, name='form_create_apparel'),
    path('update/rambut/<str:id_rambut>',form_update_rambut, name='form_update_rambut'),
    path('update_rambut/',update_rambut, name='update_rambut'),
    path('update/mata/',form_update_mata, name='form_update_mata'),
    path('update/rumah/',form_update_rumah, name='form_update_rumah'),
    path('update/barang/',form_update_barang, name='form_update_barang'),
    path('update/apparel/',form_update_apparel, name='form_update_apparel'),
    path('delete_rambut/<str:id_rambut>',delete_rambut, name='delete_rambut'),
]