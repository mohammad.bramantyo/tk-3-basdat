from collections import namedtuple
from urllib import response
from django.db import connection
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def logincheck(request):
    if(request.session.is_empty()):
        return HttpResponseRedirect('/')

def read_list_koleksi(request):
    logincheck(request)

    return render(request,'list_koleksi.html')

def create_koleksi(request):
    logincheck(request)
        
    return render(request,'create_koleksi.html')

def read_rambut(request):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
            select id_koleksi,tipe,harga,
            case when id_koleksi not in
            (select id_rambut from  tokoh)
            then true
            else false
            end
            as delete
            from rambut,koleksi where id=id_koleksi
            order by id_koleksi asc;
        """)
        hasil = namedtuplefetchall(c)
        response = {'hasil':hasil}

        return render(request, "read_rambut.html", response)

def read_mata(request):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
            select id_koleksi, warna, harga,
            case when id_koleksi not in
            (select id_mata from  tokoh)
            then true
            else false
            end
            as delete
            from mata,koleksi where id=id_koleksi
            order by id_koleksi asc;
        """)
        hasil = namedtuplefetchall(c)
        response = {'hasil':hasil}

        return render(request, "read_mata.html", response)

def read_rumah(request):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
            select id,nama,harga as harga_jual,harga_beli,kapasitas_barang,
            case when k.id not in
            (select id_rumah from tokoh)
            then true
            else false
            end
            as delete
            from koleksi k, koleksi_jual_beli kjb, rumah
            where k.id = kjb.id_koleksi and kjb.id_koleksi=rumah.id_koleksi
            order by id asc;
        """)
        hasil = namedtuplefetchall(c)
        response = {'hasil':hasil}

        return render(request, "read_rumah.html", response)

def read_barang(request):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
            select id,nama,harga as harga_jual,harga_beli,tingkat_energi,
            case when k.id not in
            (select id_barang from menggunakan_barang)
            then true
            else false
            end
            as delete
            from koleksi k, koleksi_jual_beli kjb, barang b
            where k.id = kjb.id_koleksi and kjb.id_koleksi=b.id_koleksi
            order by id asc;
        """)
        hasil = namedtuplefetchall(c)
        response = {'hasil':hasil}

        return render(request, "read_barang.html", response)

def read_apparel(request):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
            select id,nama,harga as harga_jual,harga_beli,kategori_apparel,nama_pekerjaan,
            case when a.id_koleksi not in
            (select id_koleksi from menggunakan_apparel)
            then true
            else false
            end
            as delete
            from koleksi k, koleksi_jual_beli kjb, apparel a
            where k.id = kjb.id_koleksi and kjb.id_koleksi=a.id_koleksi
            order by id asc;
        """)
        hasil = namedtuplefetchall(c)
        response = {'hasil':hasil}

        return render(request, "read_apparel.html", response)

# create form
def form_create_rambut(request):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select * from rambut order by id_koleksi desc limit 1;
        """)
        
        last_id_koleksi = ""
        for record in c:
            last_id_koleksi = record[0]
        
        int_id = int(last_id_koleksi[-3:])
        int_id+=1
        new_id = "RB"+"{0:03}".format(int_id)
        response = {"new_id":new_id}

    return render(request,'create_rambut.html',response)

def form_create_mata(request):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select * from mata order by id_koleksi desc limit 1;
        """)
        
        last_id_koleksi = ""
        for record in c:
            last_id_koleksi = record[0]
        
        int_id = int(last_id_koleksi[-3:])
        int_id+=1
        new_id = "MT"+"{0:03}".format(int_id)
        response = {"new_id":new_id}

    return render(request,'create_mata.html',response)

def form_create_rumah(request):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select * from rumah order by id_koleksi desc limit 1;
        """)
        
        last_id_koleksi = ""
        for record in c:
            last_id_koleksi = record[0]
        
        int_id = int(last_id_koleksi[-3:])
        int_id+=1
        new_id = "RM"+"{0:03}".format(int_id)
        response = {"new_id":new_id}

    return render(request,'create_rumah.html',response)

def form_create_barang(request):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select * from barang order by id_koleksi desc limit 1;
        """)
        
        last_id_koleksi = ""
        for record in c:
            last_id_koleksi = record[0]
        
        int_id = int(last_id_koleksi[-2:])
        int_id+=1
        new_id = "BRG"+"{0:02}".format(int_id)
        response = {"new_id":new_id}

    return render(request,'create_barang.html',response)

def form_create_apparel(request):
    logincheck(request)
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select * from apparel order by id_koleksi desc limit 1;
        """)
        
        last_id_koleksi = ""
        for record in c:
            last_id_koleksi = record[0]
        
        int_id = int(last_id_koleksi[-2:])
        int_id+=1
        new_id = "APP"+"{0:02}".format(int_id)

        c.execute("""
        select * from kategori_apparel;
        """)
        kategori = namedtuplefetchall(c)

        c.execute("""
        select nama from pekerjaan;
        """)
        pekerjaan = namedtuplefetchall(c)

        response = {"new_id":new_id, "kategori":kategori, "pekerjaan":pekerjaan}

    return render(request,'create_apparel.html',response)

#Create koleksi
def create_rambut(request):
    if(request.session.is_empty() or request.session['role']=="pemain"):
        return HttpResponseRedirect('/')
    
    if(request.method=="POST"):
        harga_jual = int(request.POST["harga_jual"])
        tipe_rambut = request.POST["tipe_rambut"]
        id_koleksi = request.POST["id"]

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        insert into koleksi values
        ('{}',{})
        """.format(id_koleksi,harga_jual))

        c.execute("""
        insert into rambut values
        ('{}','{}');
        """.format(id_koleksi,tipe_rambut))

    return HttpResponseRedirect('/koleksi/read/rambut/')

def create_mata(request):
    if(request.session.is_empty() or request.session['role']=="pemain"):
        return HttpResponseRedirect('/')
    
    if(request.method=="POST"):
        harga_jual = int(request.POST["harga_jual"])
        warna_mata = request.POST["warna_mata"]
        id_koleksi = request.POST["id"]

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        insert into koleksi values
        ('{}',{})
        """.format(id_koleksi,harga_jual))

        c.execute("""
        insert into mata values
        ('{}','{}');
        """.format(id_koleksi,warna_mata))

    return HttpResponseRedirect('/koleksi/read/mata/')

def create_rumah(request):
    if(request.session.is_empty() or request.session['role']=="pemain"):
        return HttpResponseRedirect('/')
    
    if(request.method=="POST"):
        id_koleksi = request.POST["id"]
        nama_rumah = request.POST["nama_rumah"]
        harga_jual = int(request.POST["harga_jual"])
        harga_beli = int(request.POST["harga_beli"])
        kapasitas_barang = int(request.POST["kapasitas_barang"])

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        insert into koleksi values
        ('{}',{})
        """.format(id_koleksi,harga_jual))

        c.execute("""
        insert into koleksi_jual_beli values
        ('{}',{},'{}');
        """.format(id_koleksi,harga_beli,nama_rumah))

        c.execute("""
        insert into rumah values
        ('{}',{});
        """.format(id_koleksi,kapasitas_barang))
        
    return HttpResponseRedirect('/koleksi/read/rumah/')

def create_barang(request):
    if(request.session.is_empty() or request.session['role']=="pemain"):
        return HttpResponseRedirect('/')
    
    if(request.method=="POST"):
        id_koleksi = request.POST["id"]
        nama_barang = request.POST["nama_barang"]
        harga_jual = int(request.POST["harga_jual"])
        harga_beli = int(request.POST["harga_beli"])
        tingkat_energi = int(request.POST["tingkat_energi"])
    
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        insert into koleksi values
        ('{}',{})
        """.format(id_koleksi,harga_jual))

        c.execute("""
        insert into koleksi_jual_beli values
        ('{}',{},'{}');
        """.format(id_koleksi,harga_beli,nama_barang))

        c.execute("""
        insert into barang values
        ('{}',{});
        """.format(id_koleksi,tingkat_energi))

    return HttpResponseRedirect('/koleksi/read/barang/')

def create_apparel(request):
    if(request.session.is_empty() or request.session['role']=="pemain"):
        return HttpResponseRedirect('/')
    
    if(request.method=="POST"):
        id_koleksi = request.POST["id"]
        nama_apparel = request.POST["nama_apparel"]
        harga_jual = int(request.POST["harga_jual"])
        harga_beli = int(request.POST["harga_beli"])
        warna_apparel = request.POST["warna_apparel"]
        kategori_apparel = request.POST["kategori_apparel"]
        nama_pekerjaan = request.POST["nama_pekerjaan"]
        

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        insert into koleksi values
        ('{}',{})
        """.format(id_koleksi,harga_jual))

        c.execute("""
        insert into koleksi_jual_beli values
        ('{}',{},'{}');
        """.format(id_koleksi,harga_beli,nama_apparel))

        c.execute("""
        insert into apparel values
        ('{}','{}','{}','{}');
        """.format(id_koleksi,nama_pekerjaan,kategori_apparel,warna_apparel))

    return HttpResponseRedirect('/koleksi/read/apparel/')

# update form
def form_update_rambut(request,id_rambut):
    logincheck(request)

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select id,tipe,harga from koleksi, rambut
        where id=id_koleksi and id_koleksi='{}';
        """.format(id_rambut))

        hasil = namedtuplefetchall(c)
        response={'hasil':hasil}

    return render(request,'update_rambut.html',response)

def form_update_mata(request):
    logincheck(request)

    return render(request,'update_mata.html')
def form_update_rumah(request):
    logincheck(request)

    return render(request,'update_rumah.html')
def form_update_barang(request):
    logincheck(request)

    return render(request,'update_barang.html')
def form_update_apparel(request):
    logincheck(request)
    
    return render(request,'update_apparel.html')

#Update koleksi
def update_rambut(request):
    logincheck(request)

    if(request.method=="POST"):
        harga_jual = int(request.POST["harga_jual"])
        tipe_rambut = request.POST["tipe_rambut"]
        id_koleksi = request.POST["id"]
    
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        update rambut
        set tipe='{}'
        where id_koleksi='{}';
        """.format(tipe_rambut,id_koleksi))

        c.execute("""
        update koleksi
        set harga={}
        where id='{}';
        """.format(harga_jual,id_koleksi))
    return HttpResponseRedirect('/koleksi/read/rambut/')

#Delete koleksi
def delete_rambut(request,id_rambut):
    logincheck(request)

    with connection.cursor() as c:
        c.execute('set search_path to thecims;')
        c.execute("""
        delete from rambut where id_koleksi='{}';        
        """.format(id_rambut))

        c.execute("""
        delete from koleksi where id='{}';        
        """.format(id_rambut))
        

    return HttpResponseRedirect('/koleksi/read/rambut/')