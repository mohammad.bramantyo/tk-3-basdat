from django.urls import path
from .views import *

urlpatterns=[
    path('read/',read,name='read'),
    path('create/',create,name='create'),
    path('create_koleksi_tokoh',create_koleksi_tokoh,name='create_koleksi_tokoh'),
]