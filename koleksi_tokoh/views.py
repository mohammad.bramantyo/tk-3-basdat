from collections import namedtuple
from django.db import connection
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read(request):
    if(request.session.is_empty()):
        return HttpResponseRedirect('/')
    role = request.session['role']

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        if(role=="admin"):
            c.execute("""select * 
            from koleksi_tokoh;
            """)
        elif(role=="pemain"):
            c.execute("""     
                select id_koleksi,username_pengguna,kt.nama_tokoh,
                case
                when id_koleksi like 'RB%' and id_koleksi not in (select id_rambut from tokoh where nama=kt.nama_tokoh) then true
                when id_koleksi like 'MT%' and id_koleksi not in (select id_mata from tokoh where nama=kt.nama_tokoh) then true
                when id_koleksi like 'RM%' and id_koleksi not in (select id_rumah from tokoh where nama=kt.nama_tokoh) then true
                when id_koleksi like 'BRG%' and id_koleksi not in (select mb.id_barang from menggunakan_barang mb where mb.nama_tokoh=kt.nama_tokoh) then true
                when id_koleksi like 'APP%' and id_koleksi not in (select ma.id_koleksi from menggunakan_apparel ma where ma.nama_tokoh=kt.nama_tokoh) then true
                else false
                end
                as jual
                from koleksi_tokoh kt
                where username_pengguna='""" +request.session['username'] +"""';
            """ )

        hasil = namedtuplefetchall(c)
        response = {"hasil":hasil}

        return render(request,"read_koleksi_tokoh.html",response)

def create(request):
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""select nama from tokoh where username_pengguna='{}';""".format(request.session['username']))
        list_tokoh = namedtuplefetchall(c)

        c.execute("select id from koleksi;")
        list_koleksi = namedtuplefetchall(c)
        response = {'tokoh':list_tokoh,'koleksi':list_koleksi}

    return render(request,'create_koleksi_tokoh.html',response)

def create_koleksi_tokoh(request):
    if(request.session.is_empty()):
        return HttpResponseRedirect('/')

    if request.method == "POST":
        nama_tokoh = request.POST['nama_tokoh']
        id_koleksi = request.POST['id_koleksi']

        with connection.cursor() as c:
            c.execute('set search_path to thecims;')
            c.execute("""
            insert into koleksi_tokoh values
            ('{}','{}','{}')
            """.format(id_koleksi,request.session['username'],nama_tokoh))
        return HttpResponseRedirect('/koleksi_tokoh/read/')
