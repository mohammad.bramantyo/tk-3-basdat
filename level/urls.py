from django.urls import path
from .views import create, read, delete, update, create_validation, update_validation

urlpatterns = [
    path('create/', create, name='create-level'),
    path('create-validation/', create_validation, name='validation-create-level'),
    path('', read, name='read-level'),
    path('delete/<str:level>', delete, name='delete-level'),
    path('update/<int:level>/<int:xp>', update, name='update-level'),
    path('update-validation/<int:level>/<int:xp>', update_validation, name='validation-update-level')

]