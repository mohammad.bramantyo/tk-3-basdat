from urllib import response
from django.shortcuts import redirect, render

# Create your views here.
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read(request):
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select l.level, l.xp,
        case when not exists (select 1 from tokoh as t where t.level = l.level)
        then 'true' else 'false' end as deletable
        from level as l;
        """)
        hasil = namedtuplefetchall(c)

    response = {
        'hasil': hasil,
    }

    return render(request, 'level.html', response)

def create(request):
    return render(request, 'create-level.html', {})

def create_validation(request):
    level = request.POST.get("level")
    xp = request.POST.get("xp")
    if level == "" or xp == "" :
        return render(request, 'create-level.html', {'message' :"Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"});
        
    level_data = {
        "level" : request.POST.get("level"),
        "xp" : request.POST.get("xp")
    }

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select level from level
        """)
        hasil = namedtuplefetchall(c)
        for data in hasil :
            if str(data.level) == level_data['level'] :
                return render(request, 'create-level.html', {'message' : "Data sudah terdaftar. Harap masukkan data lain"});
        level_int = int(level_data['level'])
        xp_int = int(level_data['xp'])
        c.execute("""
        insert into level values ('%d', '%d')
        """%(level_int, xp_int))
        return redirect("/level")

def update(request, level, xp):
    response = {
        "level" : level,
        "xp" : xp,
        "message" : ""
    }
    return render(request, 'update-level.html', response)

def update_validation(request, level, xp):
    xp_data = request.POST.get('xp')
    response = {
        "level" : level,
        "xp" : xp,
        "message" : ""
    }
    if xp_data == "":
        response['message'] = "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"
        return render(request, 'update-level.html', response)
    xp_int = int(xp_data)
    
    if len(xp_data) == 0 :
        response['message'] = "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"
        return render(request, 'update-level.html', response)
    else:
        with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute("""
            update level set xp = '%d' where level = '%d'
            """%(xp_int, level))
            return redirect("/level")
    

def delete(request, level):
    int_level = int(level)
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        delete from level where level = '%d'
        """%(int_level))
    return redirect('/level')