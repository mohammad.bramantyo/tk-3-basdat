from django import forms

class Login_Form(forms.Form):
    username = forms.CharField(label='username', max_length=30, required=True, widget=forms.TextInput(attrs={
        'id':'form',
        'placeholder':'Masukkan username'
    }))

    password = forms.CharField(label='password', max_length=20, required=True, widget=forms.PasswordInput(attrs={
        'id':'form',
        'placeholder':'Password'
    }))

class Signup_Pemain(forms.Form):
    username_attrs={
        'class':'form-control',
        'placeholder': 'Masukkan username Anda'
    }
    password_attrs={
        'class':'form-control',
        'placeholder': 'Masukkan password Anda'
    }
    email_attrs={
        'class':'form-control',
        'placeholder': 'text@example.com'
    }
    nomor_hp_attrs={
        'class':'form-control',
        'placeholder': 'xxxx-xxxx-xxxx'
    }

    username=forms.CharField(
        label='username',
        required=True,
        max_length=50,
        widget=forms.TextInput(attrs=username_attrs))

    password=forms.CharField(
        label='password',
        required=True,
        max_length=50,
        widget=forms.PasswordInput(attrs=password_attrs))

    email=forms.EmailField(
        label='email',
        required=True,
        max_length=20,
        widget=forms.EmailInput(attrs=email_attrs))

    no_hp=forms.CharField(
        label='no_hp',
        required=True,
        max_length=12,
        widget=forms.TextInput(attrs=nomor_hp_attrs))


# class Signup_Admin(forms.Form):
#     username_attrs={
#         'class':'form-control',
#         'placeholder': 'username Anda'
#     }
#     password_attrs={
#         'class':'form-control',
#         'placeholder': 'password Anda'
#     }

#     username=forms.CharField(
#         label='username',
#         required=True,
#         max_length=50,
#         widget=forms.TextInput(attrs=username_attrs))

#     password=forms.CharField(
#         label='password',
#         required=True,
#         max_length=20,
#         widget=forms.PasswordInput(attrs=password_attrs))
