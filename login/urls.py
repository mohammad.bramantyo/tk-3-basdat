from django.urls import path
from . import views

urlpatterns = [
	path('login/',views.login,name="login"),
	path('',views.home,name="home"),
	path('signup_pemain/',views.signup_pemain,name="signup_pemain"),
	path('signup_admin/',views.signup_admin,name="signup_admin"),
    path('profile/',views.profile,name="profile"),
	path('logout/',views.logout,name="logout"),
]
