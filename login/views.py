from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from django.contrib import messages

def profile(request):
	try:
		role=request.session["role"]
	except:
		return redirect("/")
	
	stored = {}
	if role == "pemain":
		username = request.session["username"]

		stored["username"] = username
		stored["email"] = request.session["email"]
		stored["no_hp"] = request.session["no_hp"]

		with connection.cursor() as c:
			c.execute("set search_path to thecims; select koin from pemain where username = '{}'".format(username))
			exist = c.fetchone()
			koin = exist[0]

		stored["koin"] = koin
		stored["role"] = "pemain"
	else:
		stored["username"] = request.session["username"]
		stored["role"] = "admin"
	return render(request, "profile.html", stored)

def home(request):
	if request.session.is_empty() == False:
		return redirect("/profile")
	else:
		return render(request, "home.html")

def login(request):
	if request.method == "POST":
		username = request.POST["username"]
		with connection.cursor() as c:
			c.execute("set search_path to thecims; select username from akun where username = '{}'".format(username))
			exist = c.fetchone()
		
		if (not exist):
			request.session['none'] = True
		else:
			with connection.cursor() as c:
				password =  request.POST["password"]
				c.execute("select * from admin where username='{}' and password='{}'".format(username, password))
				res = c.fetchall()

				if len(res) > 0:
					request.session["username"] = username
					request.session["role"] = "admin"
					return redirect("/profile")
				else:
					c.execute("select * from pemain where username='{}' and password='{}'".format(username, password))
					res = c.fetchall()

					if len(res) > 0:
						request.session["username"] = username
						request.session["email"] = res[0][1]
						request.session["no_hp"] = res[0][3]
						request.session["koin"] = res[0][4]
						request.session["role"] = "pemain"
						return redirect("/profile")
					else:
						return render(request, "login.html", {"error":"Kombinasi username atau password salah!"})
	return render(request, "login.html", {"error": ""})

def logout(request):
    request.session.flush()
    return redirect("/")

def signup_pemain(request):
	if request.method == "POST":
		try:
			username = request.POST["username"]
			email = request.POST["email"]
			no_hp = request.POST["no_hp"]
			password = request.POST["password"]

			with connection.cursor() as c:
				c.execute("set search_path to thecims")
				c.execute(f"""
				insert into akun values ('{username}')
				""")

				c.execute("set search_path to thecims")
				c.execute(f"""
				insert into pemain values 
				('{username}', '{email}', '{password}', '{no_hp}', 0)
				""")

				c.execute("select * from pemain where username='{}' and password='{}'".format(username, password))
				res = c.fetchone()

				request.session["username"] = res[0]
				request.session["email"] = res[1]
				request.session["no_hp"] = res[3]
				request.session["koin"] = res[4]
				request.session["role"] = "pemain"
				return redirect("/profile")
		except IntegrityError:
			messages.add_message(request, messages.WARNING, "Email sudah pernah terdaftar. Silahkan gunakan email yang lain")
	return render(request, "signup_pemain.html")

def signup_admin(request):
	if request.method == "POST":
		try:
			username = request.POST["username"]
			password = request.POST["password"]

			with connection.cursor() as c:
				c.execute("set search_path to thecims")
				c.execute(f"""
				insert into akun values ('{username}')
				""")

				c.execute("set search_path to thecims")
				c.execute(f"""
				insert into admin values 
				('{username}', '{password}')
				""")
				request.session["username"] = username
				request.session["role"] = "admin"
				return redirect('/profile')
		except IntegrityError:
			messages.add_message(request, messages.WARNING, "Pendaftaran admin gagal. Silahkan cek kembali data yang dimasukkan.")
	return render(request, "signup_admin.html")
