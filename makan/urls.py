from django.urls import path
from . import views
# from django.contrib.auth.views import LoginView,LogoutView
urlpatterns = [
	path('read-admin/',views.read_admin,name="read-admin"),
    path('read-pemain/',views.read_pemain,name="read-pemain"),
    path('create/',views.create,name="create"),
]
