from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from collections import namedtuple
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.utils import timezone


# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def read_admin(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")
        
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select *
        from makan;
        """)
        hasil = namedtuplefetchall(c)
        
        return render(request, "makan-read-admin.html", {"hasil":hasil})

def read_pemain(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")

    uname = request.session['username']
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute(f"""
        select *
        from makan
        where username_pengguna = '{uname}';
        """)
        hasil = namedtuplefetchall(c)
        
        return render(request, "makan-read-pemain.html", {"hasil":hasil})


@csrf_exempt
def create(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")

    if request.method == "POST":
        try:
            time = timezone.now()
            username = request.session['username']
            nama_tokoh = request.POST['nama_tokoh']
            nama_makanan = request.POST['nama_makanan']
            with connection.cursor() as c:
                c.execute("set search_path to thecims")
                c.execute(f"""
                insert into makan
                values (
                    '{username}',
                    '{nama_tokoh}',
                    '{time}',
                    '{nama_makanan}')
                """)
                return redirect("/makan/read-pemain")

        except Exception as e:
            msg = str(e).split()
            error = msg[0:11]
            listToStr = ' '.join(map(str, error))
            messages.add_message(request, messages.WARNING, listToStr)

    with connection.cursor() as c:
        data = {}
        username = request.session['username']
        c.execute("set search_path to thecims")
        c.execute(f"""
        select nama from tokoh
        where username_pengguna = '{username}'
        """)
        data["list_n_tokoh"] = namedtuplefetchall(c)
        
        c.execute("select nama from makanan")
        data["list_n_makanan"] = namedtuplefetchall(c)

        return render(request, "makan-create.html", data)