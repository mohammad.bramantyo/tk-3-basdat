from django.urls import path
from . import views
# from django.contrib.auth.views import LoginView,LogoutView
urlpatterns = [
	path('read/',views.read,name="read"),
    path('create/',views.create,name="create/"),
    path('update/<str:nama>/',views.update,name="update/"),
	path('delete/<str:nama>/',views.delete,name="delete/"),
]
