from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from collections import namedtuple
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages


# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def read(request):
    role = request.session["role"]
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select *,
        case when not exists (select 1 from makan where nama_makanan = m.nama)
        then 'true' else 'false' end as deletable
        from makanan m;
        """)
        hasil = namedtuplefetchall(c)
        
    if role == "pemain":
        return render(request, "makanan-read-pemain.html", {"hasil":hasil})
    else:
        return render(request, "makanan-read-admin.html", {"hasil":hasil})

@csrf_exempt
def create(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    if request.method == "POST":
        try:
            nama_makanan = request.POST['nama_makanan']
            harga = request.POST['harga']
            t_energi = request.POST['tingkat_energi']
            t_kelaparan = request.POST['tingkat_kelaparan']

            with connection.cursor() as c:
                c.execute("set search_path to thecims")
                c.execute(f"""
                insert into makanan values
                ('{nama_makanan}', '{harga}', '{t_energi}', '{t_kelaparan}')
                """)
                return redirect('/makanan/read')

        except IntegrityError:
            messages.add_message(request, messages.WARNING, "Data yang diisikan sudah ada pada DB, silahkan cek data terlebih dahulu")

    return render(request, 'makanan-create.html')

@csrf_exempt
def update(request, nama):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    if request.method == "POST":
        try:
            nama_makanan = request.POST['nama_makanan']
            harga = request.POST['harga']
            t_energi = request.POST['tingkat_energi']
            t_kelaparan = request.POST['tingkat_kelaparan']

            with connection.cursor() as c:
                c.execute("set search_path to thecims")
                c.execute(f"""
                update makanan set (nama, harga, tingkat_energi, tingkat_kelaparan) = ('{nama_makanan}', '{harga}', '{t_energi}', '{t_kelaparan}')
                where nama = '{nama_makanan}'
                """)
                return redirect('/makanan/read')

        except IntegrityError:
            messages.add_message(request, messages.WARNING, "Data yang di-update tidak ada pada DB, silahkan cek data terlebih dahulu")

    with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute(f"""
            select * 
            from makanan
            where nama = '{nama}'
            """)
            hasil = namedtuplefetchall(c)
            return render(request, "makanan-update.html", {'hasil':hasil})

def delete(request, nama):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    with connection.cursor() as c:
        c.execute('set search_path to thecims;')
        c.execute(f"""
        delete 
        from makanan 
        where nama = '{nama}' 
        """)

    return redirect('read')

