from django.urls import path
from .views import create, read, delete, getApparel, create_validation

urlpatterns = [
    path('create/', create, name='create-menggunakan-apparel'),
    path('create_validation/', create_validation, name='create-validation-menggunakan-apparel'),
    path('', read, name='read-menggunakan-apparel'),
    path('delete/<str:tokoh>/<str:app>', delete, name='delete-menggunakan-apparel'),
    path('<str:tokoh>/' , getApparel, name="apparel")
]