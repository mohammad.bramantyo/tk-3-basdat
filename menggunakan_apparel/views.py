from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.contrib import messages

# Create your views here.
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read(request):
    role = request.session["role"]
    if role == "admin" :
        with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute("""
            select username_pengguna, nama_tokoh, kjb.nama, warna_apparel, nama_pekerjaan, kategori_apparel from menggunakan_apparel as ma left outer join koleksi_jual_beli as kjb on ma.id_koleksi = kjb.id_koleksi left join apparel as a on kjb.id_koleksi = a.id_koleksi;
            """)
            hasil = namedtuplefetchall(c)
    else :
        with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute("""
            select username_pengguna, nama_tokoh, kjb.nama, warna_apparel, nama_pekerjaan, kategori_apparel from menggunakan_apparel as ma left outer join koleksi_jual_beli as kjb on ma.id_koleksi = kjb.id_koleksi left join apparel as a on kjb.id_koleksi = a.id_koleksi
            where username_pengguna = '{}'
            """.format(request.session["username"]))
            hasil = namedtuplefetchall(c)

    response = {
        'hasil': hasil,
    }

    return render(request, 'menggunakan_apparel.html', response)

def create_validation(request):
    tokoh = request.POST['tokoh']
    app = request.POST['apparel']
    username = request.session['username']

    if tokoh == "" or app == "" :
        messages.error(request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
        return redirect('/menggunakan_apparel/create')
    
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        insert into menggunakan_apparel values ('{}', '{}', '{}')
        """.format(username, tokoh, app))
    
    return redirect('/menggunakan_apparel')
    


def create(request):
    username = request.session.get('username')
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select distinct t.nama as nama from tokoh as t, koleksi_tokoh as kt where t.username_pengguna = kt.username_pengguna and t.nama = kt.nama_tokoh and kt.username_pengguna = '{}';
        """.format(username))
        hasil_tokoh = namedtuplefetchall(c)

    response = {
        'hasil_tokoh': hasil_tokoh,
    }
    return render(request, 'create-menggunakan-apparel.html', response)

def getApparel(request, tokoh) :
    if request.is_ajax():
        username = request.session.get('username')
        with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute("""
            select distinct kt.id_koleksi, kjb.nama from tokoh as t, koleksi_tokoh as kt, koleksi_jual_beli as kjb where t.username_pengguna = kt.username_pengguna and t.nama = kt.nama_tokoh and kt.id_koleksi = kjb.id_koleksi and kt.id_koleksi like 'APP%' and kt.username_pengguna = '{}' and kt.nama_tokoh = '{}';
            """.format(username, tokoh))
            hasil_koleksi = namedtuplefetchall(c)
        return JsonResponse({'data' : hasil_koleksi})

def delete(request, tokoh, app):
    nama_pengguna = request.session["username"]
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select id_koleksi from koleksi_jual_beli where nama = '{}'
        """.format(app))
        hasil = namedtuplefetchall(c)
    id_apparel = hasil[0].id_koleksi

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        delete from menggunakan_apparel where username_pengguna = '{}' and nama_tokoh = '{}' and id_koleksi = '{}'
        """.format(nama_pengguna, tokoh, id_apparel))
    return redirect('/menggunakan_apparel')
