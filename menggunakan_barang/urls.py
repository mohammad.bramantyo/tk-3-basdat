from django.urls import path
from.views import create, read, index, get_barang, create_menggunakan_barang

urlpatterns = [
    path('', index, name='home-menggunakan-barang'),
    path('create/', create, name='create-menggunakan-barang'),
    path('read/', read, name='read-menggunakan-barang'),
    path('get-barang/<str:nama_tokoh>', get_barang, name='get-barang'),
    path('create-menggunakan-barang/', create_menggunakan_barang, name='submit-create-menggunakan-barang')
]
