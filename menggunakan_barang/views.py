from collections import namedtuple
from datetime import datetime, timezone, timedelta
from django.contrib import messages
from django.db import connection
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect


def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def read(request):
    if request.session.is_empty():
        return redirect("/")

    with connection.cursor() as c:
        role = request.session['role']
        username = request.session['username']
        if role == 'admin':
            c.execute('set search_path to thecims;')
            c.execute("""
            select username_pengguna, nama_tokoh, nama, waktu
            from menggunakan_barang, koleksi_jual_beli
            where id_barang = id_koleksi;
            """)
            hasil = namedtuplefetchall(c)

        elif role == 'pemain':
            c.execute('set search_path to thecims;')
            c.execute("""
            select nama_tokoh, nama, waktu
            from menggunakan_barang, koleksi_jual_beli
            where id_barang=id_koleksi and username_pengguna='{}';
            """.format(username))
            hasil = namedtuplefetchall(c)

    response = {
        'role': role,
        'hasil': hasil,
    }

    return render(request, 'menggunakan-barang-read.html', response)


def create(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")

    with connection.cursor() as c:
        username = request.session['username']
        c.execute('set search_path to thecims;')
        c.execute("select nama from tokoh where username_pengguna='{}';".format(username))
        hasil = namedtuplefetchall(c)
        print(hasil)

    response = {
        'hasil': hasil,
        'notifikasi': '',
    }

    return render(request, 'menggunakan-barang-create.html', response)


# source = https://www.youtube.com/watch?v=VEN2H3wGOW4&ab_channel=PiusGeek
def get_barang(request, nama_tokoh):
    if request.is_ajax():
        with connection.cursor() as c:
            username = request.session['username']
            c.execute('set search_path to thecims;')
            c.execute("""select nama, c.id_koleksi
            from koleksi_jual_beli a, koleksi_tokoh b, barang c
            where a.id_koleksi=b.id_koleksi and
            b.id_koleksi=c.id_koleksi and
            (username_pengguna, nama_tokoh) = ('{}', '{}')
            """.format(username, nama_tokoh))
            barang = namedtuplefetchall(c)
            print(barang)
        return JsonResponse({'data': barang})
    return HttpResponse('Wrong request')


def create_menggunakan_barang(request):
    if request.method == 'POST':
        nama_tokoh = request.POST['tokoh']
        id_barang = request.POST['barang']

        if nama_tokoh == "" or id_barang == "":
            return HttpResponse('Data tidak valid')

        username = request.session['username']

        with connection.cursor() as c:
            c.execute('set search_path to thecims;')
            c.execute(
                "select energi from tokoh where (username_pengguna, nama)=('{}','{}');".format(username, nama_tokoh))
            energi = namedtuplefetchall(c)[0].energi

        with connection.cursor() as c:
            c.execute('set search_path to thecims;')
            c.execute("select tingkat_energi from barang where id_koleksi='{}';".format(id_barang))
            tingkat_energi = namedtuplefetchall(c)[0].tingkat_energi

        if energi < tingkat_energi:
            messages.error(request, 'Energi tokoh tidak mencukupi sehingga barang tidak dapat digunakan')
            return redirect('create-menggunakan-barang')

        with connection.cursor() as c:
            dt = datetime.now(timezone.utc) + timedelta(hours=7)
            c.execute('set search_path to thecims;')
            c.execute("""
            insert into menggunakan_barang values
            ('{}', '{}', '{}', '{}');
            """.format(username, nama_tokoh, dt, id_barang))

    return redirect('read-menggunakan-barang')


def index(request):
    if request.session.is_empty():
        return redirect("/")

    return render(request, 'menggunakan-barang-home.html')
