from django.urls import path
from . import views

# from django.contrib.auth.views import LoginView,LogoutView
urlpatterns = [
	path('read-pemain/',views.read_pemain,name="read/"),
	path('read-admin/',views.read_admin,name="read/"),
    path('create/',views.create,name="create/"),
	path('update/<str:n_tokoh>/<str:n_misi>',views.update,name="update/"),
]
