from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from collections import namedtuple
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages

# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read_pemain(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")    

    username_pengguna = request.session["username"]
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select *
        from misi m, menjalankan_misi_utama mu
        where m.nama = mu.nama_misi
        and mu.username_pengguna = '{}';
        """.format(username_pengguna))
        hasil = namedtuplefetchall(c)
        return render(request, "menjalankan_misi-pemain-read.html", {"hasil":hasil})

def read_admin(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/") 

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select *
        from misi m, menjalankan_misi_utama mu
        where m.nama = mu.nama_misi;
        """)
        hasil = namedtuplefetchall(c)
        return render(request, "menjalankan_misi-admin-read.html", {"hasil":hasil})

@csrf_exempt
def create(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")

    if request.method == "POST":
        try:
            with connection.cursor() as c:
                nama_tokoh = request.POST['nama_tokoh']
                nama_misi = request.POST['nama_misi']
                s_energi = ''
                s_hub_sos = ''
                s_kelaparan = ''
                t_energi = ''
                t_hub_sos = ''
                t_kelaparan = ''

                c.execute("set search_path to thecims")
                c.execute(f"""
                select *
                from misi
                where nama = '{nama_misi}'
                """)
                mission = namedtuplefetchall(c)
                for row in mission:
                    s_energi = row[4]
                    s_hub_sos = row[5]
                    s_kelaparan = row[6]
                    break

                c.execute("set search_path to thecims")
                c.execute(f"""
                select *
                from tokoh
                where nama = '{nama_tokoh}'
                """)
                mission = namedtuplefetchall(c)
                for row in mission:
                    t_energi = row[5]
                    t_kelaparan = row[6]
                    t_hub_sos = row[7]
                    break
                
                if (int(t_energi) >= int(s_energi)) & (int(t_hub_sos) >= int(s_hub_sos)) & (int(t_kelaparan) <= int(s_kelaparan)):
                    c.execute("set search_path to thecims")
                    c.execute(f"""
                    insert into menjalankan_misi_utama
                    values (
                    '{request.session['username']}',
                    '{nama_tokoh}',
                    '{nama_misi}',
                    'In Progress')
                    """)
                    return redirect("/menjalankan_misi_utama/read-pemain")
                else:
                    messages.add_message(request, messages.WARNING, "Syarat misi utama tidak mencukupi sehingga misi utama tidak dapat dijalankan")

        except Exception as e:
            msg = str(e).split()
            error = msg[0:10]
            listToStr = ' '.join(map(str, error))
            messages.add_message(request, messages.WARNING, listToStr)


    with connection.cursor() as c:
        data = {}
        username = request.session['username']
        c.execute("set search_path to thecims")
        c.execute(f"""
        select nama from tokoh
        where username_pengguna = '{username}'
        """)
        data["list_n_tokoh"] = namedtuplefetchall(c)
        
        c.execute("select nama_misi from misi_utama")
        data["list_n_misi"] = namedtuplefetchall(c)
        return render(request, 'menjalankan_misi-create.html', data)

def update(request, n_tokoh, n_misi):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")

    if request.method == "POST":
        try:
            uname = request.session['username']
            nama_tokoh = request.POST['nama_tokoh']
            nama_misi = request.POST['nama_misi']
            status = request.POST['status']

            with connection.cursor() as c:
                c.execute("set search_path to thecims")
                c.execute(f"""
                update menjalankan_misi_utama
                set (username_pengguna, nama_tokoh, nama_misi, status) = 
                ('{uname}', '{nama_tokoh}', '{nama_misi}', '{status}')
                where username_pengguna = '{uname}' and nama_tokoh = '{nama_tokoh}' and nama_misi = '{nama_misi}'
                """)
                return redirect('/menjalankan_misi_utama/read-pemain')

        except IntegrityError:
            messages.add_message(request, messages.WARNING, "Data yang di-update tidak ada pada DB, silahkan cek data terlebih dahulu")

    with connection.cursor() as c:
            uname = request.session['username']
            c.execute("set search_path to thecims")
            c.execute(f"""
            select * 
            from menjalankan_misi_utama
            where username_pengguna = '{uname}' and nama_tokoh = '{n_tokoh}' and nama_misi = '{n_misi}'
            """)
            hasil = namedtuplefetchall(c)
            return render(request, 'menjalankan_misi-update.html', {'hasil': hasil})