from django.urls import path
from . import views
# from django.contrib.auth.views import LoginView,LogoutView
urlpatterns = [
	path('read/',views.read,name="read/"),
	path('read/detail/<str:nama>/',views.read_detail,name="detail/"),
    path('create/',views.create,name="create/"),
	path('delete/<str:nama>/',views.delete,name="delete/"),
]
