from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from collections import namedtuple
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages


# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def read(request):
    if request.session.is_empty():
        return redirect("/")
    else:
        role = request.session["role"]
        with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute("""
            select m.*,
            case when not exists 
            (select 1 
            from menjalankan_misi_utama 
            where m.nama = nama_misi)
            then 'true' else 'false' end as deletable
            from misi m, misi_utama mu
            where m.nama = mu.nama_misi
            """)
            hasil = namedtuplefetchall(c)

        if role == "pemain":
            return render(request, "misi_utama-read-pemain.html", {"hasil":hasil})
        else:
            return render(request, "misi_utama-read-admin.html", {"hasil":hasil})

def read_detail(request,nama):
    if request.session.is_empty():
        return redirect("/")

    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select nama, efek_energi, efek_hubungan_sosial, efek_kelaparan, 
        syarat_energi, syarat_hubungan_sosial, syarat_kelaparn,
        completion_time, reward_koin, reward_xp
        from misi 
        where nama = '{}';""".format(nama))

        hasil = namedtuplefetchall(c)

    return render(request, "detail.html", {"hasil":hasil})

def create(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    if request.method == "POST":
        try:
            nama_misi = request.POST['nama_misi']
            e_energi = request.POST['efek_energi']
            e_hub_sos = request.POST['efek_hubungan_sosial']
            e_kelaparan = request.POST['efek_kelaparan']
            s_energi = request.POST['syarat_energi']
            s_hub_sos = request.POST['syarat_hubungan_sosial']
            s_kelaparan = request.POST['syarat_kelaparan']
            comp_time = request.POST['completion_time']
            r_koin = request.POST['reward_koin']
            r_xp = request.POST['reward_xp']
            desc = request.POST['deskripsi']

            with connection.cursor() as c:
                c.execute("set search_path to thecims")
                c.execute(f"""
                insert into misi values
                ('{nama_misi}', '{e_energi}', '{e_hub_sos}', '{e_kelaparan}', 
                '{s_energi}', '{s_hub_sos}', '{s_kelaparan}', '{comp_time}', 
                '{r_koin}', '{r_xp}', '{desc}')
                """)

                c.execute("set search_path to thecims")
                c.execute(f"""
                insert into misi_utama values
                ('{nama_misi}')
                """)
                return redirect('/misi_utama/read')

        except IntegrityError:
            messages.add_message(request, messages.WARNING, "Data yang diisikan sudah ada pada DB, silahkan cek data terlebih dahulu")

    return render(request, 'misi_utama-create.html')


def delete(request, nama):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    with connection.cursor() as c:
        c.execute('set search_path to thecims;')
        c.execute(f"""
        delete 
        from misi_utama 
        where nama_misi = '{nama}' 
        """)

        c.execute('set search_path to thecims;')
        c.execute(f"""
        delete 
        from misi 
        where nama = '{nama}' 
        """)

    return redirect('/misi_utama/read')

