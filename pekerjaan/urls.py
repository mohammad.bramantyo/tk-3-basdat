from django.urls import path
from .views import create, read, update, delete, index, create_pekerjaan, update_pekerjaan

urlpatterns = [
    path('', index, name='home-pekerjaan'),
    path('create/', create, name='create-pekerjaan'),
    path('read/', read, name='read-pekerjaan'),
    path('update/<str:pekerjaan>/<int:base_honor>', update, name='update-pekerjaan'),
    path('delete/<str:pekerjaan>', delete, name='delete-pekerjaan'),
    path('create-pekerjaan/', create_pekerjaan, name='submit-create-pekerjaan'),
    path('update-pekerjaan/', update_pekerjaan, name='submit-update-pekerjaan'),
]