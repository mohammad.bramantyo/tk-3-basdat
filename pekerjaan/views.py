from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple


# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def read(request):
    if request.session.is_empty():
        return redirect("/")

    role = request.session['role']
    with connection.cursor() as c:
        c.execute("set search_path to thecims;")
        c.execute("""
        select nama, base_honor,
        case when not exists (select 1 from tokoh where pekerjaan = p.nama) and
        not exists (select 1 from bekerja where nama_pekerjaan = p.nama) and
        not exists (select 1 from apparel where nama_pekerjaan = p.nama)
        then 'true' else 'false' end as deletable
        from pekerjaan p
        order by nama asc;
        """)
        hasil = namedtuplefetchall(c)

    response = {
        'role': role,
        'hasil': hasil,
    }

    return render(request, 'pekerjaan-read.html', response)


def create(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    return render(request, 'pekerjaan-create.html')


def create_pekerjaan(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    if request.method == "POST":
        pekerjaan = request.POST['pekerjaan']
        base_honor = request.POST['base-honor']

        with connection.cursor() as c:
            c.execute('set search_path to thecims;')
            c.execute("""
            insert into pekerjaan values
            ('{}', {})
            """.format(pekerjaan, base_honor))

    return redirect('read-pekerjaan')


def update(request, pekerjaan, base_honor):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    response = {
        'nama_pekerjaan': pekerjaan,
        'base_honor': base_honor,
    }

    return render(request, 'pekerjaan-update.html', response)


def update_pekerjaan(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    if request.method == "POST":
        pekerjaan = request.POST['pekerjaan']
        base_honor = request.POST['base-honor']

        with connection.cursor() as c:
            c.execute('set search_path to thecims;')
            c.execute("update pekerjaan set base_honor={} where nama='{}'".format(base_honor, pekerjaan))

    return redirect('read-pekerjaan')


def delete(request, pekerjaan):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'pemain':
        return redirect("/")

    with connection.cursor() as c:
        c.execute('set search_path to thecims;')
        c.execute("delete from pekerjaan where nama='{}';".format(pekerjaan))

    return redirect('read-pekerjaan')


def index(request):
    if request.session.is_empty():
        return redirect("/")

    return render(request, 'pekerjaan-home.html')
