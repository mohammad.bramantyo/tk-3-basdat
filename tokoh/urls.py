from django.urls import path
from . import views

# from django.contrib.auth.views import LoginView,LogoutView
urlpatterns = [
    path('read/', views.read, name="read"),
    path('detail/<str:nama>/', views.read_detail, name="detail"),
    path('detail-admin/<str:username>/<str:nama>/', views.read_detail_admin, name="detail"),
    path('create/', views.create, name="create/"),
    path('update/<str:nama>/', views.update, name="update/"),
]
