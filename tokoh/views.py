from django.shortcuts import redirect, render
from django.db import connection, IntegrityError
from collections import namedtuple
from django.contrib import messages

# Create your views here.
def namedtuplefetchall(cursor):
    """Return all rows from a cursor as a namedtuple"""
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read(request):
    try:
        role = request.session["role"]
    except:
        return redirect("/")

    if role == "pemain":
        username= request.session["username"]
        with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute("""
            select *
            from tokoh
            where username_pengguna = '{}';""".format(username))
            hasil = namedtuplefetchall(c)
            return render(request, "tokoh-read-pemain.html", {"hasil":hasil})
    else:
        with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute("""
            select *
            from tokoh 
            """)
            hasil = namedtuplefetchall(c)
            return render(request, "tokoh-read-admin.html", {"hasil":hasil})
            
def read_detail(request, nama):
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select nama, id_rambut, id_mata, id_rumah, warna_kulit, pekerjaan
        from tokoh 
        where nama = '{}';""".format(nama))

        hasil = namedtuplefetchall(c)

    return render(request, "detail-tokoh.html", {"hasil":hasil})

def read_detail_admin(request, username, nama):
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute(f"""
        select nama, id_rambut, id_mata, id_rumah, warna_kulit, pekerjaan
        from tokoh 
        where username_pengguna = '{username}' 
        and nama = '{nama}'
        """)

        hasil = namedtuplefetchall(c)

    return render(request, "detail-tokoh.html", {"hasil":hasil})

def create(request):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")

    if request.method == "POST":
        try:
            username = request.session['username']
            nama = request.POST['nama']
            jk = request.POST['jenis_kelamin']
            warna_kulit = request.POST['warna']
            pekerjaan = request.POST['pekerjaan']

            with connection.cursor() as c:
                c.execute("set search_path to thecims")
                c.execute(f"""
                insert into tokoh values
                ('{username}', '{nama}', '{jk}', 'Aktif', 0, 100, 0, 0, '{warna_kulit}', 
                1,  'jenius', '{pekerjaan}', 'RB001', 'MT001', 'RM001')
                """)
                return redirect('/tokoh/read')

        except IntegrityError:
            messages.add_message(request, messages.WARNING, "Data yang diisikan sudah ada pada DB, silahkan cek data terlebih dahulu")
    
    with connection.cursor() as c:
        data = {}
        username = request.session['username']
        c.execute("set search_path to thecims")
        c.execute("select distinct jenis_kelamin from tokoh")
        data["list_jenis_kelamin"] = namedtuplefetchall(c)
        
        c.execute("select * from warna_kulit")
        data["list_warna_kulit"] = namedtuplefetchall(c)

        c.execute("select nama from pekerjaan")
        data["list_pekerjaan"] = namedtuplefetchall(c)

        return render(request, "tokoh-create.html", data)


def update(request, nama):
    if request.session.is_empty():
        return redirect("/")

    if request.session['role'] == 'admin':
        return redirect("/")

    if request.method == "POST":
        username = request.session['username']
        nama = request.POST['nama']
        id_rambut = request.POST['id_rambut']
        id_mata = request.POST['id_mata']
        id_rumah = request.POST['id_rumah']

        with connection.cursor() as c:
            c.execute('set search_path to thecims;')
            c.execute(f"""
            update tokoh set
            id_rambut='{id_rambut}',
            id_mata = '{id_mata}',
            id_rumah = '{id_rumah}' 
            where username_pengguna = '{username}' and nama='{nama}'
            """)
            return redirect('/tokoh/read')

    with connection.cursor() as c:
        data = {}
        data['nama'] = nama

        username = request.session['username']   
        c.execute("set search_path to thecims")
        c.execute(f"""
        select id_koleksi 
        from koleksi_tokoh
        where id_koleksi like 'RB%' and username_pengguna = '{username}' and nama_tokoh = '{nama}'
        """)
        data['rambut'] = namedtuplefetchall(c)

        c.execute("set search_path to thecims")
        c.execute(f"""
        select id_koleksi 
        from koleksi_tokoh
        where id_koleksi like 'MT%' and username_pengguna = '{username}' and nama_tokoh = '{nama}'
        """)
        data['mata'] = namedtuplefetchall(c)

        c.execute("set search_path to thecims")
        c.execute(f"""
        select id_koleksi 
        from koleksi_tokoh
        where id_koleksi like 'RM%' and username_pengguna = '{username}' and nama_tokoh = '{nama}'
        """)
        data['rumah'] = namedtuplefetchall(c)

        return render(request, 'tokoh-update.html', data)


