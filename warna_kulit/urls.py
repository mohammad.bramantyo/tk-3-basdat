from django.urls import path
from .views import create, read, delete, validation_create

urlpatterns = [
    path('create/', create, name='create-warna-kulit'),
    path('create-validation/', validation_create, name='validation-create-warna-kulit'),
    path('', read, name='read-warna-kulit'),
    path('delete/<str:kode>', delete, name='delete-warna-kulit'),
]