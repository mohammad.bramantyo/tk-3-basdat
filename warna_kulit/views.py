from django.shortcuts import redirect, render

# Create your views here.
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read(request):
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        select kode,
        case when not exists (select 1 from tokoh as t where t.warna_kulit = wk.kode)
        then 'true' else 'false' end as deletable
        from warna_kulit as wk;
        """)
        hasil = namedtuplefetchall(c)

    response = {
        'hasil': hasil,
    }

    return render(request, 'warna-kulit.html', response)

def create(request):
    return render(request, 'create-warna-kulit.html', {'message' : ""})

def validation_create(request):
    kode =  request.POST.get("kode")
    if kode == "" :
        return render(request, 'create-warna-kulit.html', {'message' : "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    warna_kulit = {
        "kode" : kode
    }

    if len(warna_kulit['kode']) != 7 :
        return render(request, 'create-warna-kulit.html', {'message' : "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    else :
        with connection.cursor() as c:
            c.execute("set search_path to thecims")
            c.execute("""
            select kode from warna_kulit
            """)
            hasil = namedtuplefetchall(c)
            for kode in hasil :
                if kode.kode == warna_kulit['kode'] :
                    return render(request, 'create-warna-kulit.html', {'message' : "Data sudah terdaftar. Harap masukkan data lain"});
            c.execute("""
            insert into warna_kulit values ('%s')
            """%(warna_kulit['kode']))
            return redirect('/warna_kulit')

def delete(request, kode):
    with connection.cursor() as c:
        c.execute("set search_path to thecims")
        c.execute("""
        delete from warna_kulit where kode = '%s'
        """%(kode))
    return redirect('/warna_kulit')